import logo from './logo.svg';
import React from "react";
import Style from './App.module.css';
import {Header} from './Heder/header'
import {MessageList} from "./MessageList/MessageList";
import {MessageInput} from './MessageInput/MessageInput'
const App=(props)=> {
  return (<div className={Style.App}>

          <div className={Style.Header}>
              <Header Chat2={props.Chat1}>

              </Header>
          </div  >
          <div className={Style.MessageList}>
          <MessageList Chat2={props.Chat1}>

          </MessageList>
          </div>
          <div className={Style.MessageInput}>
              <MessageInput>

              </MessageInput>
          </div>
          <div>

          </div>

      </div>

  );
}

export default App;
