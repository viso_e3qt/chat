import React from "react";
import Style from './header.module.css'
export const Header=(props)=>{
    const currDate = new Date().toLocaleDateString();
    const currTime = new Date().toLocaleTimeString();
    var arr=new Array;
    for (let step = 0; step < props.Chat2.length; step++) {
        arr.push(props.Chat2[step].userId)  ;
    }



    var count = arr.reduce(function(values, v) {
        if (!values.set[v]) {
            values.set[v] = 1;
            values.count++;
        }
        return values;
    }, { set: {}, count: 0 }).count;


    return(
        <div className={Style.header}>
            <div className={Style.header_title}>
                Слоник
            </div>
            <div className={Style.header_users_count}>
                   люди:{count}
            </div>
            <div className={Style.header_messages_count}>
               Сообщений: {props.Chat2.length}
            </div>
            <div className={Style.header_last_message_date}>
                 {currDate} {currTime}
            </div>
        </div>
    );
}