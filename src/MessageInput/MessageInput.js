import React from "react";
import Style from './MessageInput.module.css'
export const MessageInput=(props)=>{
    let login=React.createRef();

    let clic = () => {
        let log=login.current.value;
        this.props=log;


    }
    // this.state={
    //     neim:""
    // };
    return(

        <div >
                <form className={Style.message_input} onSubmit={clic}>
                    <div>
                        <input  className={Style.message_input_text}
                            type="text"
                            placeholder="Ваш текст "
                            ref={login}
                        />

                    </div>
                    <div className={Style.message_input_button}>
                        <input  type="submit" value="занести " />
                    </div>
                </form>
        </div>

    );
}