import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import store from "./Chat";
let MyRender=(stata)=> {

    ReactDOM.render(
        <React.StrictMode>
            <App Chat1={store._state.Chat} />
        </React.StrictMode>,
        document.getElementById('root')
    );
}
MyRender(store.GetState());
store.kolbekMyRender(MyRender);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

